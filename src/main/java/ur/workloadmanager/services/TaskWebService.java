/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import ur.workloadmanager.beans.push.GlobalCounterView;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.Station;
import ur.workloadmanager.database.entities.Task;

/**
 * REST Web Service
 *
 * @author naddl1337
 */
@Path("task")
@RequestScoped
public class TaskWebService {

    @Context
    private UriInfo context;

    @Inject
    DbManager dbm;

    @Inject
    GlobalCounterView counter;

    /**
     * Creates a new instance of TaskWebService
     */
    public TaskWebService() {
    }

    /**
     * Retrieves representation of an instance of
     * ur.workloadmanager.services.TaskWebService
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getTaskList() {
        return dbm.getTaskDAO().findAll();
    }

    @POST
    @Path("insert")
    @Consumes(MediaType.APPLICATION_JSON)
    public void SetTaskList(Task task) throws InterruptedException {
        dbm.getTaskDAO().persist(task);
        counter.increment();
    }
    @POST
    @Path("delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTask(Task task) throws InterruptedException {
        dbm.getTaskDAO().remove(task);
        counter.increment();
    }
    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    public void createTask(TaskWrapper taskwrapper) throws ParseException {
        Employee employee = dbm.getEmployeeDAO().findById(taskwrapper.getEmployee());
        Station section = dbm.getStationDAO().findById(taskwrapper.getSection());

        Task task = new Task();
        task.setDescription(taskwrapper.getName());
        task.setEmployeeId(employee);
        task.setStationObj(section);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        Date start = sdf.parse(taskwrapper.getStartDate() + " " + taskwrapper.getStartTime());
        Date end = sdf.parse(taskwrapper.getEndDate() + " " + taskwrapper.getEndTime());

        task.setStart(start);
        task.setEnd(end);
        dbm.getTaskDAO().persist(task);
        counter.increment();

    }

    /**
     * PUT method for updating or creating an instance of TaskWebService
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
