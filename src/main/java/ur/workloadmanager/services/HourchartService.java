/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services;

import java.io.Serializable;
import ur.workloadmanager.services.hourservice.DataSet;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import ur.workloadmanager.beans.UserBean;
import ur.workloadmanager.beans.push.GlobalCounterView;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Task;
import ur.workloadmanager.services.hourservice.ChartWrapper;

@Path("hourchart")
@SessionScoped
public class HourchartService implements Serializable {

    @Context
    private UriInfo context;

    @Inject
    UserBean userBean;

    @Inject
    DbManager dbm;

    /**
     * Creates a new instance of HourchartService
     */
    public HourchartService() {
    }

    /**
     * Retrieves representation of an instance of
     * ur.workloadmanager.services.HourchartService
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ChartWrapper getXml() {
        List<Task> tasks = dbm.getTaskDAO().findTasksForEmployee(userBean.getUser());

        ChartWrapper chartWrapper = new ChartWrapper();

        for (Task task : tasks) {
            chartWrapper.addDataSet(new DataSet(task));
        }

        return chartWrapper;
    }

    /**
     * PUT method for updating or creating an instance of HourchartService
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
