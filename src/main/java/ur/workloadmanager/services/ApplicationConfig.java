/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author naddl1337
 * finger weg :D aber schaun deaf ma
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ur.workloadmanager.services.EmployeeWebService.class);
        resources.add(ur.workloadmanager.services.HourchartService.class);
        resources.add(ur.workloadmanager.services.StationWebService.class);
        resources.add(ur.workloadmanager.services.TaskWebService.class);
    }
    
}
