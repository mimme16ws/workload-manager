/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services.hourservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.annotation.XmlRootElement;
import ur.workloadmanager.database.entities.Task;

@XmlRootElement
public class DataSet {
    
    private String label = "Stunden";
    private String fillColor = "rgba(151,187,205,0.2)";
    private String strokeColor = "rgba(151,187,205,1)";
    private String pointColor = "rgba(151,187,205,1)";
    private String pointStrokeColor = "#fff";
    private String pointHighlightFill = "#fff";
    private String pointHighlightStroke = "rgba(151,187,205,1)";
    private List<Integer> data = new ArrayList<>();
    
    private DataSet() {
    }
    
    public DataSet(Task task) {
        for (int i = 0; i < 12; i++) {
            data.add(0);
        }
        
        Date start = task.getStart();
        int month = start.getMonth();
        
        int hours = getDateDiff(start, task.getEnd(), TimeUnit.HOURS);
        
        data.set(month, data.get(month) + hours);
    }
    
    public String getLabel() {
        return label;
    }
    
    public void setLabel(String label) {
        this.label = label;
    }
    
    public String getFillColor() {
        return fillColor;
    }
    
    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }
    
    public String getStrokeColor() {
        return strokeColor;
    }
    
    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }
    
    public String getPointColor() {
        return pointColor;
    }
    
    public void setPointColor(String pointColor) {
        this.pointColor = pointColor;
    }
    
    public String getPointStrokeColor() {
        return pointStrokeColor;
    }
    
    public void setPointStrokeColor(String pointStrokeColor) {
        this.pointStrokeColor = pointStrokeColor;
    }
    
    public String getPointHighlightFill() {
        return pointHighlightFill;
    }
    
    public void setPointHighlightFill(String pointHighlightFill) {
        this.pointHighlightFill = pointHighlightFill;
    }
    
    public String getPointHighlightStroke() {
        return pointHighlightStroke;
    }
    
    public void setPointHighlightStroke(String pointHighlightStroke) {
        this.pointHighlightStroke = pointHighlightStroke;
    }
    
    public List<Integer> getData() {
        return data;
    }
    
    public void setData(List<Integer> data) {
        this.data = data;
    }
    
    void addData(DataSet dataset) {
        List<Integer> datalistadd = dataset.getData();
        for (int i = 0; i < datalistadd.size(); i++) {
            if (datalistadd.get(i) > 0) {
                data.set(i, data.get(i) + datalistadd.get(i));
            }
        }
        
    }
    
    private int getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return (int) timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
}
