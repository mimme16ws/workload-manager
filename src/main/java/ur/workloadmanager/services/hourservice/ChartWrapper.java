/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services.hourservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChartWrapper {

    private String[] labels = new String[]{"Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};
    private List<DataSet> datasets = new ArrayList<>();

    public ChartWrapper() {
        datasets.add(null);
    }

    public void addDataSet(DataSet dataset) {
        if (datasets.size() == 2) {
            DataSet set = datasets.get(1);
            set.addData(dataset);
        } else {
            datasets.add(dataset);
        }
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public List<DataSet> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DataSet> datasets) {
        this.datasets = datasets;
    }

}
