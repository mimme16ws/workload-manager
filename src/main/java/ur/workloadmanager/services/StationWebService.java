/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.services;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Station;

/**
 * REST Web Service
 *
 * @author naddl1337
 */
@Path("station")
@RequestScoped
public class StationWebService {

    @Context
    private UriInfo context;

    @Inject
    DbManager dbm;

    /**
     * Creates a new instance of StationWebService
     */
    public StationWebService() {
    }

    /**
     * Retrieves representation of an instance of
     * ur.workloadmanager.services.StationWebService
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Station> getStationList() {
        return dbm.getStationDAO().findAll();
    }

    /**
     * PUT method for updating or creating an instance of StationWebService
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
