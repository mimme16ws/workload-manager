/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ur.workloadmanager.beans.UserBean;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;

public class UserManager extends HttpServlet {

    @Inject
    UserBean userBean;

    @Inject
    DbManager dbm;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String name = request.getParameter("name");
        String value = request.getParameter("value");
        String pk = request.getParameter("pk");

        int employeeID = Integer.parseInt(pk);

        Employee emp = dbm.getEmployeeDAO().findById(employeeID);
        Employee admin = userBean.getUser();

        if (admin == null) {
            return;
        }

        if (Objects.equals(emp.getParentEmployee().getId(), admin.getId())) {

            switch (name) {
                case "Benutzername":
                    emp.setName(value);
                    break;
                case "Adresse":
                    emp.setAddress(value);
                    break;
                case "E-Mail":
                    emp.setEmail(value);
                    break;
                case "Telefon":
                    emp.setPhone(value);
                    break;
            }
            dbm.getEmployeeDAO().persist(emp);

        } else {
            System.err.println("Unberechtigter Zugriff");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
