/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.frontend_controller;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.Task;

/**
 *
 * @author naddl1337
 */
@Named
@SessionScoped
public class TaskBean implements Serializable {

    @Inject
    DbManager dbm;
    Employee emp;
    
    
    String description;

    public void createTask() {
        Task task = new Task();
        emp = new Employee();
        task.setEmployeeId(emp);
        task.setDescription(description);
        dbm.getTaskDAO().persist(task);
    }
}
