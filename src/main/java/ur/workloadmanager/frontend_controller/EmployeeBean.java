/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.frontend_controller;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.beans.UserBean;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.usermgt.Permission;

/**
 *
 * @author naddl1337
 */
@Named
@SessionScoped
public class EmployeeBean implements Serializable {

    @Inject
    DbManager dbm;

    @Inject
    UserBean userBean;

    String name = "";
    String email = "";

    public void gibAus() {
        Employee empl = new Employee();
        empl.setName("hanswurs");
        empl.setEmail("n@dine.com");
        dbm.getEmployeeDAO().persist(empl);
        dbm.getEmployeeDAO().findEmployeeByEMail("n@dine.com");
        System.out.println(dbm.getEmployeeDAO().findAll());
    }

    public void createEmployee() {
        Employee empl = new Employee();
        empl.setName(name);
        empl.setEmail(email);
        empl.setPermission(Permission.USER);
        empl.setParentEmployee(userBean.getUser());
        dbm.getEmployeeDAO().persist(empl);
        System.out.println(dbm.getEmployeeDAO().findAll());
        name = "";
        email = "";
    }



    public List<Employee> getAllEmployees() {
        return dbm.getEmployeeDAO().findAll();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
