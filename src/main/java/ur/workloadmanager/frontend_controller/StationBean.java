/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.frontend_controller;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.beans.UserBean;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Station;

/**
 *
 * @author naddl1337
 */
@Named
@SessionScoped
public class StationBean implements Serializable {

    @Inject
    DbManager dbm;

    String name = "";

    public void createStation() {
        Station stat = new Station();
        stat.setName(name);
        dbm.getStationDAO().persist(stat);
        System.out.println(dbm.getStationDAO().findAll());
        name = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
