/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.database;

import javax.ejb.Stateless;
import javax.inject.Inject;
import ur.workloadmanager.database.dao.EmployeeDAO;
import ur.workloadmanager.database.dao.StationDAO;
import ur.workloadmanager.database.dao.TaskDAO;

/**
 *
 * @author naddl1337
 */
@Stateless
public class DbManager {

    @Inject
    EmployeeDAO employeeDAO;

    @Inject
    StationDAO stationDAO;

    @Inject
    TaskDAO taskDAO;

    public EmployeeDAO getEmployeeDAO() {
        return employeeDAO;
    }

    public void setEmployeeDAO(EmployeeDAO employeeDAO) {
        this.employeeDAO = employeeDAO;
    }

    public StationDAO getStationDAO() {
        return stationDAO;
    }

    public void setStationDAO(StationDAO stationDAO) {
        this.stationDAO = stationDAO;
    }

    public TaskDAO getTaskDAO() {
        return taskDAO;
    }

    public void setTaskDAO(TaskDAO taskDAO) {
        this.taskDAO = taskDAO;
    }

}
