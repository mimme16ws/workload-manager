/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.database.dao;

import ur.workloadmanager.database.dao.easyCrit.EasyCritImpl;
import com.uaihebert.uaicriteria.UaiCriteria;
import com.uaihebert.uaicriteria.criteria.QueryType;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public abstract class GenericDAO<E, K> {

    protected Class<E> entityClass;

    @PersistenceContext
    protected EntityManager entityManager;

    @PostConstruct
    public void init() {
        this.entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected UaiCriteria<E> getCrit() {
        return new EasyCritImpl<>(entityManager, entityClass, QueryType.REGULAR);
    }

//    insert und update
    public E persist(E entity) {
        return entityManager.merge(entity);
    }
// delete 
    public void remove(E entity) {
        entityManager.remove(entityManager.merge(entity));
    }
// select
    public E findById(K id) {
        return entityManager.find(entityClass, id);
    }

    public List<E> findAll() {
        return getCrit().getResultList();

    }

    public void flush() {
        entityManager.flush();
    }

    public long count() {
        CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(entityClass)));
        return entityManager.createQuery(cq).getSingleResult();
    }

    public void refresh(E entity) {
        entityManager.refresh(entity);
    }

    public void clear() {
        entityManager.clear();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void refreshCollection(List<E> entityCollection) {
        for (E entity : entityCollection) {
            if (!this.getEntityManager().contains(entity)) {
                this.getEntityManager().refresh(this.getEntityManager().merge(entity));
            }
        }
    }

}
