/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.database.dao;

import com.uaihebert.uaicriteria.UaiCriteria;
import java.util.List;
import javax.ejb.Stateless;
import ur.workloadmanager.database.entities.Station;

/**
 *
 * @author naddl1337
 */
@Stateless
public class StationDAO extends GenericDAO<Station, Integer> {

    public List<Station> findStationByName(String name) {
        UaiCriteria<Station> crit = getCrit();
        crit.andEquals("name", name);
        return crit.getResultList();
    }
}
