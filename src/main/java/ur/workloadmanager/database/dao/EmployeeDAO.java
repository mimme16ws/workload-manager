/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.database.dao;

import com.uaihebert.uaicriteria.UaiCriteria;
import java.util.List;
import javax.ejb.Stateless;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.logic.Hasher;

/**
 *
 * @author naddl1337
 */
@Stateless
public class EmployeeDAO extends GenericDAO<Employee, Integer> {

    public Employee findEmployeeByEMail(String email) {
        UaiCriteria<Employee> crit = getCrit();
        crit.andEquals("email", email);
        return crit.getSingleResult();
    }

    public Employee authenticate(String email, String password) {
        UaiCriteria<Employee> crit = getCrit();
        crit.andEquals("email", email);
        crit.andEquals("password", Hasher.generate(password, Hasher.HashMethod.SHA256));
        return crit.getSingleResult();
    }

    public List<Employee> findUsersForAdmin(Employee admin) {
        UaiCriteria<Employee> crit = getCrit();
        crit.andEquals("parentEmployee", admin);
        return crit.getResultList();
    }

}
