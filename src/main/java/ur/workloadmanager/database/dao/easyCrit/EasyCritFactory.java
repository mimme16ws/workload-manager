package ur.workloadmanager.database.dao.easyCrit;

import com.uaihebert.uaicriteria.UaiCriteria;
import com.uaihebert.uaicriteria.criteria.QueryType;
import javax.persistence.EntityManager;

public class EasyCritFactory {

    public static <T> UaiCriteria<T> createQueryCriteria(final EntityManager entityManager, final Class<T> classToUse) {
        return new EasyCritImpl<>(entityManager, classToUse, QueryType.REGULAR);
    }

}
