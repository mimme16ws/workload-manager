/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.database.dao;

import com.uaihebert.uaicriteria.UaiCriteria;
import java.util.List;
import javax.ejb.Stateless;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.Task;

/**
 *
 * @author naddl1337
 */
@Stateless
public class TaskDAO extends GenericDAO<Task, Integer>{
    
    public List<Task> findStationById(Integer id) {
        UaiCriteria<Task> crit = getCrit();
        crit.andEquals("id", id);
        return crit.getResultList();
    }
    public List<Task> findStationById(String name) {
        UaiCriteria<Task> crit = getCrit();
        crit.andEquals("name", name);
        return crit.getResultList();
    }
    
    public List<Task> findTasksForEmployee(Employee emp){
        UaiCriteria<Task> crit = getCrit();
        crit.andEquals("employeeId", emp);
        return crit.getResultList();
    }
    
    
}
