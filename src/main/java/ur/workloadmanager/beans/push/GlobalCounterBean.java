///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ur.workloadmanager.beans.push;
//
//import java.io.Serializable;
//import javax.enterprise.context.ApplicationScoped;
//import javax.inject.Named;
//import org.primefaces.push.EventBus;
//import org.primefaces.push.EventBusFactory;
//
//@Named
//@ApplicationScoped
//public class GlobalCounterBean implements Serializable {
//
//    private volatile int count;
//
//    public int getCount() {
//        return count;
//    }
//
//    public void setCount(int count) {
//        this.count = count;
//    }
//
//    public void increment() {
//        count++;
//
//        EventBus eventBus = EventBusFactory.getDefault().eventBus();
//        eventBus.publish("/counter", String.valueOf(count));
//    }
//}
