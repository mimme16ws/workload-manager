/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.beans;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.usermgt.Permission;

@SessionScoped
@Named
public class UserBean implements Serializable {

    @Inject
    private DbManager dbm;

    private String email;
    private String password;

    private Employee user;

    public String login() {
        user = dbm.getEmployeeDAO().authenticate(email, password);

        if (user == null) {
            return "";
        } else {

            if (user.getPermission() == Permission.ADMIN) {
                return "admin";
            }

            if (user.getPermission() == Permission.SUPERADMIN) {
                return "superadmin";
            }

            if (user.getPermission() == Permission.USER) {
                return "user";
            }
        }
        return "";
    }

    public String logout() {
        user = null;
        return "logout";
    }

    public DbManager getDbm() {
        return dbm;
    }

    public void setDbm(DbManager dbm) {
        this.dbm = dbm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getUser() {
        return user;
    }

    public void setUser(Employee user) {
        this.user = user;
    }

}
