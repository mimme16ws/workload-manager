/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.beans;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.usermgt.Permission;
import ur.workloadmanager.logic.Hasher;

@ViewScoped
@Named
public class DbFill implements Serializable {

    @Inject
    DbManager dbm;

    public void fill() {
        Employee emp = dbm.getEmployeeDAO().findEmployeeByEMail("admin@admin.de");
        if (emp == null) {
            generateData();
        }
    }

    private void generateData() {

        Employee admin = new Employee("Admin", "admin@admin.de", "adresse", "123456", Hasher.generate("admin", Hasher.HashMethod.SHA256), Permission.ADMIN);
        Employee user = new Employee("User", "user@user.de", "adresse", "123456", Hasher.generate("user", Hasher.HashMethod.SHA256), Permission.USER);
        admin = dbm.getEmployeeDAO().persist(admin);

        user.setParentEmployee(admin);

        dbm.getEmployeeDAO().persist(user);

    }

}
