/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ur.workloadmanager.beans.admin;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import ur.workloadmanager.beans.UserBean;
import ur.workloadmanager.database.DbManager;
import ur.workloadmanager.database.entities.Employee;
import ur.workloadmanager.database.entities.Station;
import ur.workloadmanager.database.entities.Task;

@ViewScoped
@Named
public class UserOverviewBean implements Serializable {

    @Inject
    DbManager dbm;

    @Inject
    UserBean userBean;

    private List<Employee> employees;
    private List<Station> stations;

    @PostConstruct
    public void init() {
        employees = dbm.getEmployeeDAO().findUsersForAdmin(userBean.getUser());
        stations = dbm.getStationDAO().findAll();
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void deleteEmployee(Employee emp) {
        List<Task> tasks = dbm.getTaskDAO().findTasksForEmployee(emp);
        for (Task t : tasks) {
            dbm.getTaskDAO().remove(t);
        }
        dbm.getEmployeeDAO().remove(emp);
        employees = dbm.getEmployeeDAO().findUsersForAdmin(userBean.getUser());
        setEmployees(employees);
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public void deleteStation(Station stat) {
        dbm.getStationDAO().remove(stat);
        stations = dbm.getStationDAO().findAll();
        setStations(stations);
    }

}
