package ur.workloadmanager.logic;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author naddl1337
 */
public class Hasher {

    public static String generate(String input, HashMethod method) {
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(method.toString());

            messageDigest.reset();
            messageDigest.update(input.getBytes(Charset.forName("UTF8")));
            final byte[] resultByte = messageDigest.digest();
            return new String(Hex.encodeHex(resultByte));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Hasher.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static String generateRandom(HashMethod method) {
        return generate(generateRandomString(), method);
    }

    public static String generateRandomString() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    public enum HashMethod {

        MD5,
        SHA {

            @Override
            public String toString() {
                return "SHA-1";
            }

        },
        SHA256 {

            @Override
            public String toString() {
                return "SHA-256";
            }
        },
        SHA384 {

            @Override
            public String toString() {
                return "SHA-384";
            }

        }
    }

}
