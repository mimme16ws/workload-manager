var App = App || {};
/**
 * @namespace EventActions
 * @memberof App
 */

App.EventActions = (function () {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */


    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function saveNewTask(callback) {

        var taskname = $('#calendarAddEntry').val(),
                employeeid = $('#employeedropdown option:selected').attr('name'),
                sectionid = $('#sectiondropdown option:selected').attr('name'),
                startDate = $('#datepickfield').val(),
                startTime = $('#clockpickfield').val(),
                endDate = $('#datepickfieldEnd').val(),
                endTime = $('#clockpickfieldEnd').val(),
                task = {
                    name: taskname,
                    employee: employeeid,
                    section: sectionid,
                    startDate: startDate,
                    startTime: startTime,
                    endDate: endDate,
                    endTime: endTime
                };

        $.ajax({
            url: SiteName + "webresources/task/create",
            type: "POST",
            data: JSON.stringify(task),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                callback();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function setupTask(scheduler) {
        var popup = scheduler.that.getSectionWrap();
        $(popup).click(function (event) {
            var offset = $(this).offset(),
                    posLeft = event.pageX - offset.left,
                    period = getSelectedPeriod(scheduler),
                    periodEnd = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                    minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(periodEnd, 'minutes')),
                    start = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * (posLeft / $(this).width()));
            for (var i = 0; i < scheduler.that.getSections().length; i++) {
                if (scheduler.that.getSections()[i] != undefined)
                    $('#sectiondropdown').append($('<option name="' + scheduler.that.getSections()[i].sectionId[0] + '"></option>').val(scheduler.that.getSections()[i].name).html(scheduler.that.getSections()[i].name));
            }

            $('#datepickfield').val(zeroPad(start._d.getDate(), 2) + "." + zeroPad(start._d.getMonth() + 1, 2) + "." + start._d.getFullYear());
            $('#clockpickfield').val(zeroPad(start._d.getHours(), 2) + ":" + zeroPad(start._d.getMinutes(), 2));
            if (scheduler.that.getLock()) {
                return;
            }

//            deside to add or delete task
            if (event.target.offsetParent.className == 'time-sch-item-content') {
                var item = scheduler.that.getItems();
                if (event.target.offsetParent.getElementsByClassName('description')[0]) {
                    $('#calendarDeleteEntry').text(event.target.offsetParent.getElementsByClassName('description')[0].textContent);
                }
                $('#employeeDeleteEntry').text(event.target.offsetParent.getElementsByClassName('name')[0].textContent);
                $('#itemId').text(event.target.offsetParent.getElementsByClassName('itemId')[0].textContent);
                $('#sectionDeleteEntry').text(scheduler.that.getSections()[event.target.offsetParent.getElementsByClassName('stationid')[0].textContent].name);
                Avgrund.show("#deleteTask-popup");
            } else {
                Avgrund.show("#addTask-popup");
            }

        });
    }

    function setupEvents(itemElem, scheduler) {
        if (scheduler.that.getOptions().getAllowDragging()) {
            scheduler.that.getEventFunctions().dragEvent(itemElem, scheduler);
            scheduler.that.getEventFunctions().dropEvent(scheduler);
        }
        if (scheduler.that.getOptions().getAllowResizing()) {
            var foundHandles = scheduler.that.getEventFunctions().checkResizeHandles(itemElem);
            if (foundHandles) {
                scheduler.that.getEventFunctions().resizeEvent(itemElem, scheduler, foundHandles);
            }
        }
    }

    function resized(changedItem, oldItem) {
    }



    function movement_action(item, start, end) {
        var html;
        html = '<div>';
        html += '   <div>';
        html += '       Start: ' + start.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '   <div>';
        html += '       End: ' + end.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '</div>';
        $('.realtime-info').empty().append(html);
    }

    function movement_start() {
//        $('.realtime-info').show();
    }

    function movement_end() {
//        $('.realtime-info').hide();
    }


    function movement_action(item, start, end) {
        var html;

        html = '<div>';
        html += '   <div>';
        html += '       Start: ' + start.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '   <div>';
        html += '       End: ' + end.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '</div>';

        $('.realtime-info').empty().append(html);
    }

    function movement_start() {
//        $('.realtime-info').show();
    }

    return {
        setupEvents: setupEvents,
        movement_start: movement_start,
        movement_end: movement_end,
        movement_action: movement_action,
        resized: resized,
        setupTask: setupTask,
        saveNewTask: saveNewTask
    };
}());
