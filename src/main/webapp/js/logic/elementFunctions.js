var App = App || {};

/**
 * @namespace ElementFunctions
 * @memberof App
 */
App.ElementFunctions = (function () {

    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    function cleanUpElements(scheduler) {
        scheduler.that.getOptions().getElement().find('.ui-draggable').draggable('destroy');
        scheduler.that.getOptions().getElement().empty();
    }

    function renderSections(scheduler, sections) {
        var timeCount,
                tr,
                td,
                sectionContainer,
                i,
                time,
                timeCount = 1,
                temp,
                headers = $.makeArray(scheduler.that.getTableHeader().find('tr'));

        for (i = 0; i < headers.length; i++) {
            if (timeCount < $(headers[i]).find('.time-sch-date-header').length) {
                timeCount = $(headers[i]).find('.time-sch-date-header').length;
            }
        }

        for (i = 0; i < sections.length; i++) {
            tr = $(document.createElement('tr'))
                    .addClass('time-sch-section-row')
                    .addClass(i % 2 === 0 ? 'time-sch-section-even' : 'time-sch-section-odd')
                    .css('height', scheduler.that.getOptions().getMinRowHeight())
                    .appendTo(scheduler.that.getTableContent());
            sectionContainer = $(document.createElement('div'))
                    .addClass('time-sch-section-container')
                    .css('height', scheduler.that.getOptions().getMinRowHeight())
                    .data('section', sections[i])
                    .appendTo(scheduler.that.getSectionWrap());
            td = $(document.createElement('td'))
                    .addClass('time-sch-section time-sch-section-content')
                    .data('section', sections[i])
                    .append(sections[i].name)
                    .appendTo(tr);
            for (time = 0; time < timeCount; time++) {
                td = $(document.createElement('td'))
                        .addClass('time-sch-date time-sch-date-content')
                        .appendTo(tr);
                renderFunction.renderHeaderClasses(scheduler, td, time);
            }

            temp = scheduler.that.getSections();
            temp[sections[i].id] = {
                sectionId: [sections[i].id],
                name: sections[i].name,
                row: tr,
                container: sectionContainer
            };

            scheduler.that.setSections(temp);
        }

        scheduler.that.getSectionWrap().css({
            left: scheduler.that.getOptions().getElement().find('.time-sch-section').outerWidth()
        });
        if (scheduler.that.getOptions().getShowCurrentTime()) {
            showCurrentTimeDelimiter(scheduler);
        }
    }



    function renderContent(items, scheduler) {
        var item,
                event,
                section,
                itemElem,
                itemContent,
                minuteDiff,
                itemDiff,
                itemSelfDiff,
                eventDiff,
                calcTop,
                calcLeft,
                calcWidth,
                foundStart,
                foundEnd,
                inSection = {},
                foundPos,
                elem,
                prevElem,
                needsNewRow,
                period,
                end,
                i,
                period = getSelectedPeriod(scheduler),
                end = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(end, 'minutes'));

        if (!items.length) {
            var helper = [];
            helper[0] = items;
            items = helper;
        }
        for (i = 0; i < items.length; i++) {
            item = items[i];
            section = scheduler.that.getSections()[item.stationId];
            if (section) {
                if (!inSection[item.stationId]) {
                    inSection[item.stationId] = [];
                }

                if (item.start <= end && item.end >= scheduler.that.getOptions().getStart()) {
                    foundPos = null;
                    foundStart = moment(Math.max(item.start, scheduler.that.getOptions().getStart()));
                    foundEnd = moment(Math.min(item.end, end));
                    itemDiff = foundStart.diff(scheduler.that.getOptions().getStart(), 'minutes');
                    itemSelfDiff = Math.abs(foundStart.diff(foundEnd, 'minutes'));
                    calcTop = 0;
                    calcLeft = (itemDiff / minuteDiff) * 100;
                    calcWidth = (itemSelfDiff / minuteDiff) * 100;
                    itemElem = $(document.createElement('div'))
                            .addClass('time-sch-item ' + (item.classes ? item.classes : ''))
                            .css({
                                top: calcTop,
                                left: calcLeft + '%',
                                width: calcWidth + '%'
                            })
                            .appendTo(section.container);
                    itemContent = $(document.createElement('div'))
                            .addClass('time-sch-item-content')
                            .appendTo(itemElem);
                    if (item.name) {
                        $(document.createElement('div'))
                                .addClass('name')
                                .append(item.name)
                                .appendTo(itemContent);
                    }
                    if (item.description) {
                        $(document.createElement('div'))
                                .addClass('description')
                                .append(item.description)
                                .appendTo(itemContent);
                    }
                    if (item.stationId) {
                        $(document.createElement('div'))
                                .addClass('hide stationId')
                                .append(item.stationId)
                                .appendTo(itemContent);
                    }
                    if (item.id) {
                        $(document.createElement('div'))
                                .addClass('hide itemId')
                                .append(item.id)
                                .appendTo(itemContent);
                    }

                    if (item.events) {
                        for (var ev = 0; ev < item.events.length; ev++) {
                            event = item.events[ev];
                            eventDiff = (event.at.diff(foundStart, 'minutes') / itemSelfDiff) * 100;
                            $(document.createElement('div'))
                                    .addClass('time-sch-item-event ' + (event.classes ? event.classes : ''))
                                    .css('left', eventDiff + '%')
                                    .attr('title', event.at.format(scheduler.that.getOptions().getLowerFormat()) + ' - ' + event.label)
                                    .data('event', event)
                                    .appendTo(itemElem);
                        }
                    }

                    if (item.start >= scheduler.that.getOptions().getStart()) {
                        $(document.createElement('div'))
                                .addClass('time-sch-item-start')
                                .appendTo(itemElem);
                    }
                    if (item.end <= end) {
                        $(document.createElement('div'))
                                .addClass('time-sch-item-end')
                                .appendTo(itemElem);
                    }

                    item.Element = itemElem;
                    // Place scheduler in the current section array in its sorted position
                    for (var pos = 0; pos < inSection[item.stationId].length; pos++) {
                        if (inSection[item.stationId][pos].start > item.start) {
                            foundPos = pos;
                            break;
                        }
                    }

                    if (foundPos === null) {
                        foundPos = inSection[item.stationId].length;
                    }

                    inSection[item.stationId].splice(foundPos, 0, item);
                    itemElem.data('item', item);
                    scheduler.that.getEventActions().setupEvents(itemElem, scheduler);
                }
            }
        }

// Sort out layout issues so no elements overlap
        for (var prop in inSection) {
            section = scheduler.that.getSections()[prop];
            for (i = 0; i < inSection[prop].length; i++) {
                var elemTop, elemBottom;
                elem = inSection[prop][i];
                // If we're passed the first item in the row
                for (var prev = 0; prev < i; prev++) {
                    var prevElemTop, prevElemBottom;
                    prevElem = inSection[prop][prev];
                    prevElemTop = prevElem.Element.position().top;
                    prevElemBottom = prevElemTop + prevElem.Element.outerHeight();
                    elemTop = elem.Element.position().top;
                    elemBottom = elemTop + elem.Element.outerHeight();
                    needsNewRow =
                            (
                                    (prevElem.start <= elem.start && elem.start <= prevElem.end) ||
                                    (prevElem.start <= elem.end && elem.end <= prevElem.end)
                                    ) && (
                            (prevElemTop <= elemTop && elemTop <= prevElemBottom) ||
                            (prevElemTop <= elemBottom && elemBottom <= prevElemBottom)
                            );
                    if (needsNewRow) {
                        elem.Element.css('top', prevElemBottom + 1);
                    }
                }

                elemBottom = elem.Element.position().top + elem.Element.outerHeight() + 1;
                if (elemBottom > section.container.height()) {
                    section.container.css('height', elemBottom);
                    section.row.css('height', elemBottom);
                }
            }
        }
    }
    return {
        cleanUpElements: cleanUpElements,
        renderSections: renderSections,
        renderContent: renderContent
    };
}());
