var App = App || {};

/**
 * @namespace ContentLoad
 * @memberof App
 */
App.ContentLoad = (function () {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    function pullFromWebservice() {
        $.ajax({
            dataType: "json",
            url: SiteName + "webresources/employees",
            data: {},
            success: function (data) {
                $('#employeedropdown').empty();

                $(data).each(function (index, row) {
                    $("#show_users").append("<tr><td>" + row.name + "</td><td>" + row.email + "</td></tr>");
                    $('#employeedropdown').append($('<option name="' + row.id + '"></option>').val(row.name).html(row.name));
                });

            }
        });
    }

    function translateScheduler(scheduler, callback) {
        var transurl = SiteName + "trans.html";
        $.ajax({
            url: transurl,
            context: document.body
        }).done(function (data) {
            var jsondata = JSON.parse(data),
                    text = scheduler.that.getOptions().getText();
            text.GotoButton = jsondata[0].GotoButton;
            text.NextButtonTitle = jsondata[0].NextButtonTitle;
            text.PrevButtonTitle = jsondata[0].PrevButtonTitle;
            text.TodayButton = jsondata[0].TodayButton;
            text.TodayButtonTitle = jsondata[0].TodayButtonTitle;
            text.GotoButton = jsondata[0].GotoButton;
            text.GotoButtonTitle = jsondata[0].GotoButtonTitle;
            callback(scheduler);
        });
    }

    function refreshSections(scheduler) {
        var sectionsUrl = "sections.html";
        $.ajax({
            url: sectionsUrl,
            context: document.body
        }).done(function (data) {
            scheduler.that.getElementFunctions().renderSections(scheduler, $.parseJSON(data));
        });
    }

    function refreshContent(scheduler, itemdata, overridecache) {
        if (overridecache || itemdata == null) {
            $.ajax({
                url: SiteName + "webresources/task",
                context: document.body,
                success: function (data) {
                    var itemdata = data;
                    for (var i = 0; i < itemdata.length; i++) {
                        itemdata[i] = {
                            id: itemdata[i].id,
                            employeeId: itemdata[i].employeeId,
                            description: itemdata[i].description,
                            name: itemdata[i].employeeId.name,
                            stationObj: itemdata[i].stationObj,
                            stationId: itemdata[i].stationObj.id,
                            start: moment(itemdata[i].start),
                            end: moment(itemdata[i].end),
                            classes: 'item-status-three'
                        };
                    }
                    scheduler.that.setItems(itemdata);
                    scheduler.that.getElementFunctions().renderContent(itemdata, scheduler);
                }});
        } else {
            scheduler.that.getElementFunctions().renderContent(scheduler.that.getCachedScheduleResult(), scheduler);
        }
    }

    function getSectionsByAjax(scheduler) {
        $.ajax({
            url: SiteName + "webresources/station",
            context: document.body
        }).done(function (data) {
            renderFunction.reloadSections(data, scheduler);
        });
    }


    function deleteTask(taskId) {
        var task;

        for (var i = 0; i < scheduler.that.getItems().length; i++) {
            if (taskId == scheduler.that.getItems()[i].id)
                task = scheduler.that.getItems()[i];
        }

        $.ajax({
            url: SiteName + "webresources/task/delete",
            type: "POST",
            data: JSON.stringify(task),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                Avgrund.hide();
                scheduler.init();

            },
            error: function (data) {
                console.log(data);
            }
        });
    }



    function pushInDB(task) {
        if (task === null)
            return;

        $.ajax({
            url: SiteName + "webresources/task/insert",
            type: "POST",
            data: JSON.stringify(task),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                scheduler.that.getElementFunctions().renderContent(task, scheduler);

            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    return {
        pullFromWebservice: pullFromWebservice,
        translateScheduler: translateScheduler,
        getSectionsByAjax: getSectionsByAjax,
        pushInDB: pushInDB,
        refreshContent: refreshContent,
        refreshSections: refreshSections,
        deleteTask: deleteTask
    };
}());
