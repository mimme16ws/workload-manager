var App = App || {};
/**
 * @namespace EventFunctions
 * @memberof App
 */
App.EventFunctions = (function () {

    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    function dragEvent(itemElem, scheduler) {
        itemElem.draggable({
            helper: 'clone',
            zIndex: 1,
            appendTo: scheduler.SectionWrap,
            distance: 5,
            snap: '.time-sch-section-container',
            snapMode: 'inner',
            snapTolerance: 10,
            drag: function (event, ui) {
                var item, start, end, period, periodEnd, minuteDiff;
                period = getSelectedPeriod(scheduler);
                periodEnd = getEndOfPeriod(scheduler.that.getOptions().getStart(), period);
                minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(periodEnd, 'minutes'));
                item = $(event.target).data('item');
                start = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * (ui.helper.position().left / scheduler.that.getSectionWrap().width()));
                end = moment(start).tsAdd('minutes', Math.abs(item.start.diff(item.end, 'minutes')));
                // If the start is before the start of our calendar, add the offset
                if (item.start < scheduler.that.getOptions().getStart()) {
                    start.tsAdd('minutes', item.start.diff(scheduler.that.getOptions().getStart(), 'minutes'));
                    end.tsAdd('minutes', item.start.diff(scheduler.that.getOptions().getStart(), 'minutes'));
                }


                scheduler.that.getEventActions().movement_action(item, start, end);
            },
            start: function (event, ui) {
                $(this).hide();
                // We only want content to show, not events or resizers
                ui.helper.children().not('.time-sch-item-content').remove();
                scheduler.that.getEventActions().movement_start();

            },
            stop: function (event, ui) {
                if ($(this).length) {
//                    $(this).show();
                }
                scheduler.that.getEventActions().movement_end();
            },
            cancel: '.time-sch-item-end, .time-sch-item-start, .time-sch-item-event'
        });
    }

    function dropEvent(scheduler) {
        $('.time-sch-section-container').droppable({
            greedy: true,
            hoverClass: 'time-sch-droppable-hover',
            tolerance: 'pointer',
            drop: function (event, ui) {
                var item,
                        stationId,
                        period,
                        periodEnd,
                        minuteDiff,
                        stationName,
                        period = getSelectedPeriod(scheduler),
                        periodEnd = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                        minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(periodEnd, 'minutes')),
                        item = ui.draggable.data('item'),
                        stationId = $(this).data('section').id,
                        stationName = $(this).data('section').name,
                        start = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * (ui.helper.position().left / $(this).width())),
                        end = moment(start).tsAdd('minutes', Math.abs(item.start.diff(item.end, 'minutes')));
                // If the start is before the start of our calendar, add the offset
                if (item.start < scheduler.that.getOptions().getStart()) {
                    start.tsAdd('minutes', item.start.diff(scheduler.that.getOptions().getStart(), 'minutes'));
                    end.tsAdd('minutes', item.start.diff(scheduler.that.getOptions().getStart(), 'minutes'));
                }
                // Append original to this section and reposition it while we wait
                ui.draggable.appendTo($(this));
                ui.draggable.css({
                    left: ui.helper.position().left - $(this).position().left,
                    top: ui.helper.position().top - $(this).position().top
                });
                if (scheduler.that.getOptions().getDisableOnMove()) {
                    if (ui.draggable.data('uiDraggable')) {
                        ui.draggable.draggable('disable');
                    }
                    if (ui.draggable.data('uiResizable')) {
                        ui.draggable.resizable('disable');
                    }
                }

                var foundItem;

                for (var i = 0; i < scheduler.that.getItems().length; i++) {
                    foundItem = scheduler.that.getItems()[i];

                    if (foundItem.id === item.id) {

                        foundItem.stationObj = {
                            id: stationId,
                            name: stationName
                        };

                        foundItem.stationId = stationId;
                        foundItem.start = start;
                        foundItem.end = end;

                        scheduler.that.getItems()[i] = foundItem;
                    }
                }
                scheduler.that.getContentLoad().pushInDB(item);

            }
        });

    }

    function resizeEvent(itemElem, scheduler, foundHandles) {
        itemElem.resizable({
            handles: foundHandles,
            resize: function (event, ui) {
                var item,
                        start,
                        end,
                        period,
                        periodEnd,
                        minuteDiff,
                        period = getSelectedPeriod(scheduler),
                        periodEnd = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                        minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(periodEnd, 'minutes')),
                        item = $(this).data('item');
                if (ui.position.left !== ui.originalPosition.left) {
                    // Left handle moved
                    start = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * ($(this).position().left / scheduler.that.getSectionWrap().width()));
                    end = item.end;
                } else {
                    // Right handle moved
                    start = item.start;
                    end = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * (($(this).position().left + $(this).width()) / scheduler.that.getSectionWrap().width()));
                }
                scheduler.that.getEventActions().movement_action(item, start, end);
            },
            start: function (event, ui) {
                // We don't want any events to show
                $(this).find('.time-sch-item-event').hide();
                scheduler.that.getEventActions().movement_start();
            },
            stop: function (event, ui) {
                var item,
                        start,
                        end,
                        periodEnd,
                        minuteDiff,
                        foundItem,
                        $this = $(this),
                        period = getSelectedPeriod(scheduler),
                        periodEnd = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                        minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(periodEnd, 'minutes')),
                        item = $this.data('item');
                if (ui.position.left !== ui.originalPosition.left) {
                    // Left handle moved
                    start = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * ($this.position().left / scheduler.that.getSectionWrap().width()));
                    end = item.end;
                } else {
                    // Right handle moved
                    start = item.start;
                    end = moment(scheduler.that.getOptions().getStart()).tsAdd('minutes', minuteDiff * (($this.position().left + $this.width()) / scheduler.that.getSectionWrap().width()));
                }

                if (scheduler.that.getOptions().getDisableOnMove()) {
                    if ($this.data('uiDraggable')) {
                        $this.draggable('disable');
                    }
                    if ($this.data('uiResizable')) {
                        $this.resizable('disable');
                    }

                    $this.find('.time-sch-item-event').show();
                }

                scheduler.that.getEventActions().movement_end();


                for (var i = 0; i < scheduler.that.getItems().length; i++) {
                    foundItem = scheduler.that.getItems()[i];

                    if (foundItem.id === item.id) {
                        scheduler.that.getEventActions().resized(foundItem, scheduler.that.getItems()[i]);
                        foundItem.start = start;
                        foundItem.end = end;
                        scheduler.that.getItems()[i] = foundItem;
                    }
                }
                scheduler.that.getContentLoad().pushInDB(item);
                scheduler.that.setLock(true);
                setTimeout(function () {
                    scheduler.that.setLock(false);
                }, 1000);
                scheduler.init();
            }
        });

    }

    function checkResizeHandles(itemElem) {
        if (itemElem.find('.time-sch-item-start').length && itemElem.find('.time-sch-item-end').length) {
            return 'e, w';
        } else if (itemElem.find('.time-sch-item-start').length) {
            return 'w';
        } else if (itemElem.find('.time-sch-item-end').length) {
            return 'e';
        }
        return null;
    }



    return {
        dragEvent: dragEvent,
        dropEvent: dropEvent,
        resizeEvent: resizeEvent,
        checkResizeHandles: checkResizeHandles
    };
}());

