moment.fn.tsAdd = function (input, val) {
    var dur;
    // switch args to support add('s', 1) and add(1, 's')
    if (typeof input === 'string') {
        dur = moment.duration(+val, input);
    } else {
        dur = moment.duration(input, val);
    }
    this.tsAddOrSubtractDurationFromMoment(this, dur, 1);
    return this;
};

moment.fn.tsSubtract = function (input, val) {
    var dur;
    // switch args to support subtract('s', 1) and subtract(1, 's')
    if (typeof input === 'string') {
        dur = moment.duration(+val, input);
    } else {
        dur = moment.duration(input, val);
    }
    this.tsAddOrSubtractDurationFromMoment(this, dur, -1);
    return this;
};

// Replace the AddOrSubtract function so that zoning is not taken into account at all
moment.fn.tsAddOrSubtractDurationFromMoment = function (mom, duration, isAdding) {
    var ms = duration._milliseconds,
            d = duration._days,
            M = duration._months,
            currentDate;
    if (ms) {
        mom.milliseconds(mom.milliseconds() + ms * isAdding);
        //mom._d.setTime(+mom + ms * isAdding);
    }
    if (d) {
        mom.date(mom.date() + d * isAdding);
    }
    if (M) {
        currentDate = mom.date();
        mom.date(1)
                .month(mom.month() + M * isAdding)
                .date(Math.min(currentDate, mom.daysInMonth()));
    }
};


function showCurrentTimeDelimiter(scheduler) {
    var currentTime, currentTimeElem, minuteDiff, currentDiff, end;
    // Stop any other timeouts happening
    if (scheduler.that.ShowCurrentTimeHandle) {
        clearTimeout(scheduler.that.ShowCurrentTimeHandle);
    }

    currentTime = moment();
    end = getEndOfPeriod(scheduler.that.getOptions().getStart(), getSelectedPeriod(scheduler));
    minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(end, 'minutes'));
    currentDiff = Math.abs(scheduler.that.getOptions().getStart().diff(currentTime, 'minutes'));
    currentTimeElem = scheduler.that.getOptions().getElement().find('.time-sch-current-time');
    currentTimeElem.remove();
    if (currentTime >= scheduler.that.getOptions().getStart() && currentTime <= end) {
        currentTimeElem = $(document.createElement('div'))
                .addClass('time-sch-current-time')
                .css('left', ((currentDiff / minuteDiff) * 100) + '%')
                .attr('title', currentTime.format(scheduler.that.getOptions().getLowerFormat()))
                .appendTo(scheduler.that.SectionWrap);
    }

    // Since we're only comparing minutes, we may as well only check once every 30 seconds
    scheduler.ShowCurrentTimeHandle = setTimeout(scheduler.ShowCurrentTime, 30000);
}

function getSelectedPeriod(scheduler) {
    var period;
    for (var i = 0; i < scheduler.that.getOptions().getPeriods().length; i++) {
        if (scheduler.that.getOptions().getPeriods()[i].Name === scheduler.that.getOptions().getSelectedPeriod()) {
            period = scheduler.that.getOptions().getPeriods()[i];
            break;
        }
    }

    if (!period) {
        period = scheduler.that.getOptions().getPeriods()[0];
//        todo nk -> wenn hier selectPeriod(period.Name, scheduler); aufgerufen wird, wird die init nochmal aufgerufen -> calender wird zweimal gerendert
//        selectPeriod(period.Name, scheduler); 
    }

    return period;
}

function getEndOfPeriod(start, period) {
    return moment(start).tsAdd('minutes', period.TimeframeOverall);
}

