var renderFunction = (function () {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    function createPeriodButton(periodContainer, period, selectedPeriod, scheduler) {
        $(document.createElement('a'))
                .addClass('time-sch-period-button time-sch-button')
                .addClass(period.Name === selectedPeriod.Name ? 'time-sch-selected-button' : '')
                .attr('href', '#')
                .append(period.Label)
                .data('period', period)
                .click(function (event) {
                    event.preventDefault();
                    scheduler.that.getHeaderFunctions().selectPeriod($(this).data('period').Name, scheduler);
                }).appendTo(periodContainer);
    }

    function createHeaderButton(title, button, timeContainer, cssClass, click_callback) {
        $(document.createElement('a'))
                .addClass('time-sch-time-button ' + cssClass + ' time-sch-button')
                .attr({
                    href: '#',
                    title: title
                })
                .append(button)
                .click(click_callback)
                .appendTo(timeContainer);
    }

    function renderWrapper(scheduler) {

        //direkt in der Init nach cleanup
        scheduler.that.setWrapper($(document.createElement('div'))
                .addClass('time-sch-wrapper')
                .appendTo(scheduler.that.getOptions().getElement()));
        scheduler.that.setHeaderWrap($(document.createElement('div'))
                .addClass('time-sch-header-wrapper time-sch-clearfix')
                .appendTo(scheduler.that.getWrapper()));
        scheduler.that.setTableWrap($(document.createElement('div'))
                .addClass('time-sch-table-wrapper')
                .appendTo(scheduler.that.getWrapper()));


        //create Calendar -- nach splits
        scheduler.that.setContentHeaderWrap($(document.createElement('div'))
                .addClass('time-sch-content-header-wrap')
                .appendTo(scheduler.that.getTableWrap()));
        scheduler.that.setContentWrap($(document.createElement('div'))
                .addClass('time-sch-content-wrap')
                .appendTo(scheduler.that.getTableWrap()));
        scheduler.that.setTableHeader($(document.createElement('table'))
                .addClass('time-sch-table time-sch-table-header')
                .appendTo(scheduler.that.getContentHeaderWrap()));
        scheduler.that.setTableContent($(document.createElement('table'))
                .addClass('time-sch-table time-sch-table-content')
                .appendTo(scheduler.that.getContentWrap()));
        scheduler.that.setSectionWrap($(document.createElement('div'))
                .addClass('time-sch-section-wrapper')
                .appendTo(scheduler.that.getContentWrap()));
    }

    function renderHeaderClasses(scheduler, td, columnCount, specificHeader) {
        var trArray,
                tr,
                tdArray,
                foundTD,
                prevIndex,
                nextIndex,
                colspan,
                complete,
                isEven,
                trs = scheduler.that.getTableHeader().find('tr');
        if (specificHeader !== undefined) {
            trs = $(trs.get(specificHeader));
        }

        if (trs.length && trs.length > 0) {
            trArray = $.makeArray(trs);
            for (var trCount = 0; trCount < trArray.length; trCount++) {
                complete = false;
                nextIndex = 0;
                tr = $(trArray[trCount]);
                tdArray = $.makeArray(tr.find('.time-sch-date-header'));
                for (var tdCount = 0; tdCount < tdArray.length && !complete; tdCount++) {
                    foundTD = $(tdArray[tdCount]);
                    colspan = Number(foundTD.attr('colspan'));
                    if (colspan && !isNaN(colspan) && colspan > 0) {
                        prevIndex = (nextIndex ? nextIndex : 0);
                        nextIndex = prevIndex + colspan;
                    } else {
                        prevIndex = (nextIndex ? nextIndex : 0);
                        nextIndex = prevIndex + 1;
                    }

                    if (prevIndex === columnCount) {
                        td.addClass('time-sch-header-' + trCount + '-date-start');
                    }
                    if (nextIndex - 1 === columnCount) {
                        td.addClass('time-sch-header-' + trCount + '-date-end');
                    }

                    if (prevIndex <= columnCount && columnCount < nextIndex) {
                        complete = true;
                        isEven = tdCount % 2 === 0;
                        td.addClass('time-sch-header-' + trCount + '-date-column-' + tdCount)
                                .addClass('time-sch-header-' + trCount + '-date-' + (isEven ? 'even' : 'odd'));
                        if (foundTD.hasClass('time-sch-header-' + trCount + '-current-time')) {
                            td.addClass('time-sch-header-' + trCount + '-current-time');
                        }
                    }
                }
            }
        }
    }

    function renderCalendar(scheduler) {
        var tr,
                td,
                header,
                minuteDiff,
                splits,
                end,
                thisTime,
                prevDate,
                fThisTime,
                fPrevDate,
                currentTimeIndex,
                colspan = 0,
                period = getSelectedPeriod(scheduler),
                end = getEndOfPeriod(scheduler.that.getOptions().getStart(), period),
                minuteDiff = Math.abs(scheduler.that.getOptions().getStart().diff(end, 'minutes')),
                splits = (minuteDiff / period.TimeframePeriod);

        if (period.Classes) {
            scheduler.that.getTableWrap().toggleClass(period.Classes, true);
        }

        for (var headerCount = 0; headerCount < period.TimeframeHeaders.length; headerCount++) {
            prevDate = null;
            fPrevDate = null;
            var isEven = true;
            colspan = 0;
            currentTimeIndex = 0;
            header = period.TimeframeHeaders[headerCount];
            tr = $(document.createElement('tr'))
                    .addClass('time-sch-times time-sch-times-header-' + headerCount)
                    .appendTo(scheduler.that.getTableHeader());
            td = $(document.createElement('td'))
                    .addClass('time-sch-section upperleftborders time-sch-section-header')
                    .appendTo(tr);
            for (var i = 0; i < splits; i++) {
                thisTime = moment(scheduler.that.getOptions().getStart())
                        .tsAdd('minutes', (i * period.TimeframePeriod));
                fThisTime = thisTime.format(header);
                if (fPrevDate !== fThisTime) {
                    // If there is no prevDate, it's the Section Header
                    if (prevDate) {
                        td.attr('colspan', colspan);
                        colspan = 0;
                        if (moment() >= prevDate && moment() < thisTime) {
                            td.addClass('time-sch-header-' + headerCount + '-current-time');
                        }
                    }

                    prevDate = thisTime;
                    fPrevDate = fThisTime;
                    td = $(document.createElement('td'))
                            .data('header-row', headerCount)
                            .data('column-count', i)
                            .data('column-is-even', isEven)
                            .addClass('time-sch-date time-sch-date-header')
                            .append(fThisTime)
                            .appendTo(tr);
                    td.addClass('time-sch-header-' + headerCount + '-date-start')
                            .addClass('time-sch-header-' + headerCount + '-date-end')
                            .addClass('time-sch-header-' + headerCount + '-date-column-' + currentTimeIndex)
                            .addClass('time-sch-header-' + headerCount + '-date-' + ((currentTimeIndex % 2 === 0) ? 'even' : 'odd'));
                    for (var prevHeader = 0; prevHeader < headerCount; prevHeader++) {
                        renderHeaderClasses(scheduler, td, i, prevHeader);
                    }

                    currentTimeIndex += 1;
                }

                colspan += 1;
            }

            td.attr('colspan', colspan);
        }
        scheduler.that.getHeaderFunctions().renderHeader(scheduler);
    }

    function fillSchedule(scheduler) {
        scheduler.that.setCachedScheduleResult(scheduler.that.getItems());
        scheduler.that.getContentLoad().refreshContent(scheduler, scheduler.that.getItems());
    }


    function fillSections(scheduler, override) {
        //CACHE EMPTY OR ENFORCE RELOAD
        if (!scheduler.that.getCachedSectionResult() || override) {
            scheduler.that.getContentLoad().getSectionsByAjax(scheduler);
        }
        //RE-RENDER DATA
        else {
            this.reloadSections(scheduler.that.getCachedSectionResult(), scheduler);
        }
    }

    function reloadSections(data, scheduler) {
        scheduler.that.setCachedSectionResult(data);
        scheduler.that.getElementFunctions().renderSections(scheduler, data);
        fillSchedule(scheduler);
    }

    return {
        renderWrapper: renderWrapper,
        renderCalendar: renderCalendar,
        createPeriodButton: createPeriodButton,
        createHeaderButton: createHeaderButton,
        fillSections: fillSections,
        reloadSections: reloadSections,
        renderHeaderClasses: renderHeaderClasses
    };
}());