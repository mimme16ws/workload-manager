var App = App || {};

App.Options = function (AllowDragging, AllowResizing, MinRowHeight, MaxHeight) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */


    var that = {};
    var text = new App.Text();
    var periods = new App.Periods();


    function getPeriods() {
        return periods.Periods;
    }

    function getText() {
        return text.Text;
    }

    function getSelectedPeriod() {
        return that.SelectedPeriod;
    }

    function setSelectedPeriod(SelectedPeriod) {
        that.SelectedPeriod = SelectedPeriod;
    }

    function getStart() {
        return that.Start;
    }

    function setStart(Start) {
        that.Start = Start;
    }

    function getHeaderFormat() {
        return 'Do MMM YYYY';

    }
    function getLowerFormat() {
        return 'DD-MMM-YYYY HH:mm';
    }

    function getElement() {
        return that.Element;
    }

    function setElement(elem) {
        that.Element = elem;
    }

    function getShowCurrentTime() {
        return true;
    }

    function getShowGoto() {
        return true;
    }

    function getShowToday() {
        return true;
    }

    function getAllowDragging() {
        return AllowDragging;
    }

    function getAllowResizing() {
        return AllowResizing;
    }

    function getDisableOnMove() {
        return true;
    }

    function getMinRowHeight() {
        return MinRowHeight;
    }

    function getMaxHeight() {
        return MaxHeight;
    }



    that.getPeriods = getPeriods;
    that.getText = getText;
    that.getSelectedPeriod = getSelectedPeriod;
    that.setSelectedPeriod = setSelectedPeriod;
    that.getStart = getStart;
    that.setStart = setStart;
    that.getHeaderFormat = getHeaderFormat;
    that.getLowerFormat = getLowerFormat;
    that.getElement = getElement;
    that.setElement = setElement;
    that.getShowCurrentTime = getShowCurrentTime;
    that.getShowGoto = getShowGoto;
    that.getShowToday = getShowToday;
    that.getAllowDragging = getAllowDragging;
    that.getAllowResizing = getAllowResizing;
    that.getDisableOnMove = getDisableOnMove;
    that.getMinRowHeight = getMinRowHeight;
    that.getMaxHeight = getMaxHeight;

    return that;
};



