var App = App || {};
App.Periods = function () {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    this.Periods =
            [
                {
                    Name: '1 day',
                    Label: 'Tagesansicht',
                    TimeframePeriod: (60),
                    TimeframeOverall: (60 * 24),
                    TimeframeHeaders: [
                        'Do MMM',
                        'HH'
                    ],
                    Classes: 'period-3day'
                },
                {
                    Name: '1 week',
                    Label: 'Wochenansicht',
                    TimeframePeriod: (60 * 24),
                    TimeframeOverall: (60 * 24 * 7),
                    TimeframeHeaders: [
                        'MMM',
                        'Do'
                    ],
                    Classes: 'period-1week'
                },
                {
                    Name: '1 month',
                    Label: 'Monatsansicht',
                    TimeframePeriod: (60 * 24 * 1),
                    TimeframeOverall: (60 * 24 * 28),
                    TimeframeHeaders: [
                        'MMM',
                        'Do'
                    ],
                    Classes: 'period-1month'
                }
            ];
};
