var App = App || {};
/**
 * @namespace HeaderFunctions
 * @memberof App
 */
App.HeaderFunctions = (function () {

    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    function renderHeader(scheduler) {
        var periodContainer,
                timeContainer,
                selectedPeriod,
                end,
                period,
                periodContainer = $(document.createElement('div'))
                .addClass('time-sch-period-container'),
                timeContainer = $(document.createElement('div'))
                .addClass('time-sch-time-container'),
                title = $(document.createElement('div'))
                .addClass('time-sch-title');
        scheduler.that.HeaderWrap.empty().append(periodContainer, timeContainer, title);
        selectedPeriod = getSelectedPeriod(scheduler);
        end = getEndOfPeriod(scheduler.that.getOptions().getStart(), selectedPeriod);
        // Header needs a title
        // We take away 1 minute 
        title.text(scheduler.that.getOptions().getStart().format(scheduler.that.getOptions().getHeaderFormat()) + ' - ' + end.tsAdd('minutes', -1).format(scheduler.that.getOptions().getHeaderFormat()));
        for (var i = 0; i < scheduler.that.getOptions().getPeriods().length; i++) {
            period = scheduler.that.getOptions().getPeriods()[i];
            renderFunction.createPeriodButton(periodContainer, period, selectedPeriod, scheduler);
        }



        var timeShiftClicked = function () {
            var period;
            event.preventDefault();
            period = getSelectedPeriod(scheduler);
            if ($(this).is('.time-sch-time-button-today')) {
                scheduler.that.getOptions().Start = moment().startOf('day');
            } else if ($(this).is('.time-sch-time-button-prev')) {
                scheduler.that.getOptions().getStart().tsAdd('minutes', period.TimeframeOverall * -1);
            } else if ($(this).is('.time-sch-time-button-next')) {
                scheduler.that.getOptions().getStart().tsAdd('minutes', period.TimeframeOverall);
            }

            scheduler.init();
        };

        var gotoTimeShiftClicked = function () {
            event.preventDefault();
            $(document.createElement('input'))
                    .attr('type', 'text')
                    .css({
                        position: 'absolute',
                        left: 0,
                        bottom: 0
                    })
                    .appendTo($(this))
                    .datepicker({
                        onClose: function () {
                            $(this).remove();
                        },
                        onSelect: function (date) {
                            scheduler.that.getOptions().setStart(moment(date));
                            scheduler.init();
                        },
                        defaultDate: scheduler.that.getOptions().getStart().toDate()
                    })
                    .datepicker('show')
                    .hide();
        };


        if (scheduler.that.getOptions().getShowGoto()) {
            renderFunction.createHeaderButton(scheduler.that.getOptions().getText().GotoButtonTitle, scheduler.that.getOptions().getText().GotoButton, timeContainer, "time-sch-time-button-goto", gotoTimeShiftClicked);
        }

        if (scheduler.that.getOptions().getShowToday()) {
            renderFunction.createHeaderButton(scheduler.that.getOptions().getText().TodayButtonTitle, scheduler.that.getOptions().getText().TodayButton, timeContainer, "time-sch-time-button-today", timeShiftClicked);
        }

        renderFunction.createHeaderButton(scheduler.that.getOptions().getText().PrevButtonTitle, scheduler.that.getOptions().getText().PrevButton, timeContainer, "time-sch-time-button-prev", timeShiftClicked);
        renderFunction.createHeaderButton(scheduler.that.getOptions().getText().NextButtonTitle, scheduler.that.getOptions().getText().NextButton, timeContainer, "time-sch-time-button-next", timeShiftClicked);
    }

    function selectPeriod(name, scheduler) {
        scheduler.that.getOptions().setStart(moment().startOf('day'));
        scheduler.that.getOptions().setSelectedPeriod(name);
        scheduler.init();
    }

    return {
        renderHeader: renderHeader,
        selectPeriod: selectPeriod
    };
}());