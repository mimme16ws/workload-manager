/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var App = App || {};
App.Text = function () {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    this.Text = {
        NextButton: '',
        NextButtonTitle: 'Next period',
        PrevButton: '',
        PrevButtonTitle: 'Previous period',
        TodayButton: 'Today',
        TodayButtonTitle: 'Go to today',
        GotoButton: 'Go to',
        GotoButtonTitle: 'Go to specific date'
    };
};
