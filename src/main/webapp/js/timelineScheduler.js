var App = App || {};

App.TimeScheduler = function (dragresize) {
    "use strict";
    /* eslint-env browser, jquery */
    /* globals EventBus */

    var that = {},
            lock = false,
            MinRowHeight = 50,
            MaxHeight = 100,
            Options = new App.Options(dragresize, dragresize, MinRowHeight, MaxHeight),
            Sections = [];

    function getLock() {
        return that.lock;
    }
    function setLock(lock) {
        that.lock = lock;
    }
    function getRenderFunct() {
        return that.renderFunction;
    }
    function getElementFunctions() {
        return App.ElementFunctions;
    }
    function getEventFunctions() {
        return App.EventFunctions;
    }
    function getHeaderFunctions() {
        return App.HeaderFunctions;
    }
    function getEventActions() {
        return App.EventActions;
    }
    function getContentLoad() {
        return  App.ContentLoad;
    }
    function getWrapper() {
        return that.Wrapper;
    }
    function setWrapper(Wrapper) {
        that.Wrapper = Wrapper;
    }
    function getHeaderWrap() {
        return that.HeaderWrap;
    }
    function setHeaderWrap(HeaderWrap) {
        that.HeaderWrap = HeaderWrap;
    }
    function getTableWrap() {
        return that.TableWrap;
    }
    function setTableWrap(TableWrap) {
        that.TableWrap = TableWrap;
    }
    function getContentHeaderWrap() {
        return that.ContentHeaderWrap;
    }
    function setContentHeaderWrap(ContentHeaderWrap) {
        that.ContentHeaderWrap = ContentHeaderWrap;
    }
    function getContentWrap() {
        return that.ContentWrap;
    }
    function setContentWrap(ContentWrap) {
        that.ContentWrap = ContentWrap;
    }
    function getTableHeader() {
        return that.TableHeader;
    }
    function setTableHeader(TableHeader) {
        that.TableHeader = TableHeader;
    }
    function getTableContent() {
        return that.TableContent;
    }
    function setTableContent(TableContent) {
        that.TableContent = TableContent;
    }
    function getSectionWrap() {
        return that.SectionWrap;
    }
    function setSectionWrap(SectionWrap) {
        that.SectionWrap = SectionWrap;
    }
    function getTable() {
        return that.Table;
    }
    function getSections() {
        return Sections;
    }
    function setSections(Sections) {
        that.Sections = Sections;
    }
    function getCachedSectionResult() {
        return that.CachedSectionResult;
    }
    function setCachedSectionResult(CachedSectionResult) {
        this.CachedSectionResult = CachedSectionResult;
    }
    function getCachedScheduleResult() {
        return that.CachedScheduleResult;
    }
    function setCachedScheduleResult(CachedScheduleResult) {
        that.CachedScheduleResult = CachedScheduleResult;
    }

    function getOverridecache() {
        that.Overridecache = false;
        return that.Overridecache;
    }

    function getOptions() {
        return Options;
    }

    function getItems() {
        return that.Items;
    }

    function setItems(Items) {
        that.Items = Items;
    }

    function getSectionById(id) {
        var count = 0;
        for (var i = 0; i < getSections().length; i++) {
            if (getSections()[i] !== undefined)
                count++;

            if (count === id) {
                return getSections()[i];
            }
        }

    }

    that.getLock = getLock;
    that.setLock = setLock;
    that.getContentLoad = getContentLoad;
    that.getElementFunctions = getElementFunctions;
    that.getEventFunctions = getEventFunctions;
    that.getHeaderFunctions = getHeaderFunctions;
    that.getEventActions = getEventActions;
    that.getOptions = getOptions;
    that.setWrapper = setWrapper;
    that.getWrapper = getWrapper;
    that.getHeaderWrap = getHeaderWrap;
    that.setHeaderWrap = setHeaderWrap;
    that.getTableWrap = getTableWrap;
    that.setTableWrap = setTableWrap;
    that.getContentHeaderWrap = getContentHeaderWrap;
    that.setContentHeaderWrap = setContentHeaderWrap;
    that.getContentWrap = getContentWrap;
    that.setContentWrap = setContentWrap;
    that.getTableHeader = getTableHeader;
    that.setTableHeader = setTableHeader;
    that.getTableContent = getTableContent;
    that.setTableContent = setTableContent;
    that.getSectionWrap = getSectionWrap;
    that.setSectionWrap = setSectionWrap;
    that.getOverridecache = getOverridecache;
    that.getCachedScheduleResult = getCachedScheduleResult;
    that.setCachedScheduleResult = setCachedScheduleResult;
    that.getCachedSectionResult = getCachedSectionResult;
    that.setCachedSectionResult = setCachedSectionResult;
    that.getSections = getSections;
    that.setSections = setSections;
    that.getItems = getItems;
    that.setItems = setItems;
    that.getSectionById = getSectionById;

    function init() {
        scheduler.that.getOptions().setElement($('.calendar'));
        scheduler.that.getContentLoad().translateScheduler(this, function (scheduler) {
            scheduler.that.getContentLoad().refreshContent(scheduler, scheduler.that.getItems(), true);
            scheduler.that.getOptions().setStart(moment(scheduler.that.getOptions().getStart()));
            scheduler.that.getElementFunctions().cleanUpElements(scheduler);
            renderFunction.renderWrapper(scheduler);
            renderFunction.renderCalendar(scheduler);
            renderFunction.fillSections(scheduler, true);
            scheduler.that.getEventActions().setupTask(scheduler);
        });
    }

    start();
    function start() {
        getOptions().setStart(moment().startOf('day'));
        getOptions().setSelectedPeriod("1 month");
        getContentLoad().pullFromWebservice();

    }


    return {
        init: init,
        that: that,
        getSectionById: getSectionById
    };
};
