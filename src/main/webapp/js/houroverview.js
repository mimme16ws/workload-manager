Chart.defaults.global.pointHitDetectionRadius = 1;
Chart.defaults.global.customTooltips = function (tooltip) {

    var tooltipEl = $('#chartjs-tooltip');

    if (!tooltip) {
        tooltipEl.css({
            opacity: 0
        });
        return;
    }

    tooltipEl.removeClass('above below');
    tooltipEl.addClass(tooltip.yAlign);

    var innerHtml = '';
    for (var i = tooltip.labels.length - 1; i >= 0; i--) {
        innerHtml += [
            '<div class="chartjs-tooltip-section">',
            '	<span class="chartjs-tooltip-key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>',
            '	<span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
            '</div>'
        ].join('');
    }
    tooltipEl.html(innerHtml);

    tooltipEl.css({
        opacity: 1,
        left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
        top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
        fontFamily: tooltip.fontFamily,
        fontSize: tooltip.fontSize,
        fontStyle: tooltip.fontStyle
    });
};
var randomScalingFactor = function () {
    return Math.round(Math.random() * 100);
};

$.ajax({
    url: SiteName +"webresources/hourchart",
    success: function (data, textStatus, jqXHR) {
        var lineChartData = data;
        lineChartData.datasets[0] = {};
        var ctx2 = document.getElementById("chart2").getContext("2d");
        window.myLine = new Chart(ctx2).Line(lineChartData, {
            responsive: true
        });
    }
});

