**Eine Dokumentation ist unter Downloads als zip Datei hinterlegt.**

Wenn Sie sich als Administrator beim Workload Manager anmelden möchten, loggen Sie sich bitte mit:
Benutzer: admin@admin.de
Passwort: admin
ein.
Möchten Sie sich als User anmelden, so machen Sie dies mit:
Benutzer: user@user.de
Passwort: user

Den Default User mit dem Namen "User" bitte nicht löschen.